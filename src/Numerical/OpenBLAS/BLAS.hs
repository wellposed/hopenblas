
module Numerical.OpenBLAS.BLAS(
    dgemm
    ,sgemm
    ,cgemm
    ,zgemm) where

import Numerical.OpenBLAS.UtilsFFI    
import Numerical.OpenBLAS.BLAS.FFI 

